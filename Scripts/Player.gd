extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var screensize
export var speed = 300
var shoot_rate = 0.3

onready var bullet = preload("res://Scenes/Laser.tscn")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	screensize = get_viewport_rect().size

func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	var h_velocity = 0
	var v_velocity = 0
	
	if Input.is_action_pressed("ui_right"):
		h_velocity = 1
	if Input.is_action_pressed("ui_left"):
		h_velocity = -1
	if Input.is_action_pressed("ui_up"):
		v_velocity = -1
	if Input.is_action_pressed("ui_down"):
		v_velocity = 1
		
	position.x += h_velocity * speed * delta
	position.y += v_velocity * speed * delta
	
	position.x = clamp(position.x, 0, screensize.x)
	position.y = clamp(position.y, screensize.y*2/3, screensize.y)
	
	if Input.is_action_pressed("ui_select"):
		if(shoot_rate >= 0.3):
			var n_bullet = bullet.instance()
			n_bullet.position = position
			n_bullet.position.y -= 30
			get_node("/root/Main").add_child(n_bullet)
			shoot_rate = 0
		shoot_rate += delta